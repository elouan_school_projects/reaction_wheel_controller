#!/bin/sh
# Author : Laurent Alloza
source $HOME/.config/user-dirs.dirs
if [ -z $XDG_DESKTOP_DIR ] || [ ! -d $XDG_DESKTOP_DIR ]
then
	echo "Cannot Find Desktop folder."
	exit
fi
echo "Copying files."

# Desktop Files
mkdir -vp $XDG_DESKTOP_DIR/wheel
SRC=./wheel
DST=$XDG_DESKTOP_DIR
cp -R $SRC $DST

chmod a+x $DST/wheel/*.desktop
gio set $DST/wheel/netbeans.desktop "metadata::trusted" yes
gio set $DST/wheel/interface.desktop "metadata::trusted" yes

echo "Icon="$DST"/wheel/interface/Interface.png" >> $DST/wheel/interface.desktop
echo "Exec=env http_proxy= "$DST"/wheel/interface/Interface.AppImage" >> $DST/wheel/interface.desktop

# Netbeans Files
mkdir -vp $HOME/NetBeansProjects
SRC=./ReactionWheel
DST=$HOME/NetBeansProjects
cp -R $SRC $DST

echo "Done."

