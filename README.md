# Reaction wheel controller

Repository that contains the work done for workshop made in the Supaéro TAS-ASTRO advanced master.

The goal was to build a system that control a satellite reaction wheel. For the full project documentation see the pdf in  `./docs/subject.pdf`.

---

### Setup and build
See the `./docs/getting_started.pdf` document to install all the required software and build the project.
> The `StudentPackage.zip` archive content mentioned in this file is available in the `./external/` folder.
