// ***********************************************************
// *                I S A E - S U P A E R O                  *
// *                                                         *
// *               Reaction Wheel Application                *
// *                      Version 2021                       *
// *                                                         *
// * Student version                                         *
// ***********************************************************

#include "ReactionWheel.h"
#include "log.h"

//--------------------------------------------
// Global  declarations                      
//--------------------------------------------
#define REQUEST_MANAGER_INTERFACE_PRIORITY    33
#define WHEEL_MANAGER_INTERFACE_PRIORITY    34
#define PERIOD_MANAGER_INTERFACE_PRIORITY   35

//--------------------------------------------
// Tasks descriptors                          
//--------------------------------------------
RT_TASK request_manager_task;
RT_TASK period_manager_task;
RT_TASK wheel_manager_task;

//-------------------------------------------
//  Semaphores identifiers                   
//-------------------------------------------                    
RT_SEM StartExperiment_Semaphore;
RT_SEM ExitApplication_Semaphore;

//--------------------------------------------
//  Events identifiers
//--------------------------------------------
RT_EVENT wheel_manager_event;
#define EVENT_MASK(n) (1<<n)
#define EVENT_UPDATE_WHEEL_CONTROL EVENT_MASK(0)
#define EVENT_ACQUIRE_SENSOR EVENT_MASK(1)
#define EVENT_EXPERIENCE_FINISHED EVENT_MASK(2)
#define EVENT_INIT_LAW EVENT_MASK(3)


//--------------------------------------------
// Message queues Identifiers                 
//--------------------------------------------
#define QUEUE_MAX_SIZE 60 // The maximum theoritical value between 2 read call is 50 (100ms call * 0.02 acquire period), add 10 for margin
RT_QUEUE sample_queue;


// Global variables communication and synchronization tasks by shared memory 
//--------------------------------------------------------------------------
bool abort_flag = false;
bool finished_flag = false;


// declared in WheelHMI.c 
extern ExperimentParametersType ExperimentParameters;

//--------------------------------------------
// motor function         
//--------------------------------------------
void brake_and_reset_motor(){
    LOG(LOG_INFO, "Starting motor brake...");
    SampleType sample;
    SampleAcquisition(&sample);
    
    float max_motor_speed = sample.motorSpeed;
    //LOG(LOG_DEBUG, "max_motor_speed = %f", max_motor_speed);

    float current_command = 0;

    while ((sample.motorSpeed > 0.5) || (sample.motorSpeed < -0.5)) {
        if (max_motor_speed > 0){
            current_command = sample.motorSpeed/max_motor_speed * MAXIMUM_CURRENT_LIMIT;
        } else {
            current_command = sample.motorSpeed/max_motor_speed * MINIMUM_CURRENT_LIMIT;
        }
        //LOG(LOG_DEBUG, "motor_speed: %f, current_command = %f", sample.motorSpeed, current_command);
        ApplySetpointCurrent(current_command);
        SampleAcquisition(&sample);
    }
    HardwareReset();
    LOG(LOG_INFO, "Motor brake finished");
}


//--------------------------------------------
// request_manager task functions           
//--------------------------------------------

void request_manager_task_main()
{
    LOG(LOG_INFO, "start of request_manager_task_main");
    ManageRequest();
    LOG(LOG_INFO, "end of request_manager_task_main");
}

/** This function is called when the user press on Abort during an experiment \n
 * called from function "manageRequest" in file WheelHMI.c  
 */
void AbortExperiment(void)
{
    LOG(LOG_DEBUG, "Aborting experiment");
    abort_flag = true;
}

void StartExperiment(void)
{
    LOG(LOG_DEBUG, "Starting experiment");
    abort_flag = false;
    finished_flag = false;
    rt_sem_v(&StartExperiment_Semaphore);
}

void ReturnSensorsMeasurement()
{
    uint i;
    SampleType sample_buffer;
    float samples_list[QUEUE_MAX_SIZE * 4];

    RT_QUEUE_INFO sample_queue_info;
    rt_queue_inquire(&sample_queue, &sample_queue_info);

    /*  FIXME: HMI SEGFAULT CRASH
     *  We have a segfault crash when we have more than 25 SampleType values given to the IHM :
     *      - above 27 samples, the HMI segfault when receiving the data
     *      - at 26 it segfault on the second connection
     *  The maximum allowed acquire period to prevent crash is thus above 4ms as each HMI call for samples is done every 100ms
     *  This BUG is strange because the HMI should be able to withstand 100/2=50 SampleType values as the minimum allowed time between two sensor reading is 2 ms
     */

    for (i = 0; i < sample_queue_info.nmessages; i++) {
        int res = rt_queue_read(&sample_queue, &sample_buffer, sizeof (sample_buffer), TM_NONBLOCK);
        if (res < 0) {
            LOG(LOG_DEBUG, "Error while reading from queue: %s", res, strerror(res));
            exit(1);
        }
        samples_list[(i * 4)] = sample_buffer.motorSpeed;
        samples_list[(i * 4) + 1] = sample_buffer.platformSpeed;
        samples_list[(i * 4) + 2] = sample_buffer.platformPosition;
        samples_list[(i * 4) + 3] = sample_buffer.motorCurrent;
    }

    char terminationChar = finished_flag ? 'F' : 'S';
    LOG(LOG_INFO, "Sending %d samples (SampleType of 4 float) to the Human/Machine Interface (last sample block ? %s)", sample_queue_info.nmessages, finished_flag ? "TRUE" : " FALSE");

    WriteRealArray(terminationChar, samples_list, sample_queue_info.nmessages * 4);
}

//--------------------------------------------
// period_manager task functions           
//--------------------------------------------

void period_manager_task_main()
{
    rt_task_set_periodic(&period_manager_task, TM_NOW, 1000000);
    LOG(LOG_INFO, "start of period_manager_task_main");
    while (true) {
        LOG(LOG_INFO, "waiting for experiment start");
        rt_sem_p(&StartExperiment_Semaphore, TM_INFINITE);
        ulong period_timer = 0, experiment_timer = 0;

        int ret = rt_event_signal(&wheel_manager_event, EVENT_INIT_LAW);
        if (ret < 0) {
            LOG(LOG_ERROR, "Error: failed to send event (errno: %s)", strerror(ret));
        }

        while (ConnectionIsActive() && !abort_flag && experiment_timer < ExperimentParameters.duration) {
            rt_task_wait_period(NULL);
            
            if (period_timer % ExperimentParameters.acquisitionPeriod == 0) {
                int ret = rt_event_signal(&wheel_manager_event, EVENT_ACQUIRE_SENSOR);
                if (ret < 0) {
                    LOG(LOG_ERROR, "Error: failed to send event (errno: %s)", strerror(ret));
                }
            }
            if (period_timer % ExperimentParameters.lawPeriod == 0) {
                int ret = rt_event_signal(&wheel_manager_event, EVENT_UPDATE_WHEEL_CONTROL);
                if (ret < 0) {
                    LOG(LOG_ERROR, "Error: failed to send event (errno: %s)", strerror(ret));
                }
            }

            if (period_timer >= maxval(ExperimentParameters.acquisitionPeriod, ExperimentParameters.lawPeriod)) {
                period_timer = 0;
            }

            period_timer++;
            experiment_timer++;
        }

        ret = rt_event_signal(&wheel_manager_event, EVENT_EXPERIENCE_FINISHED);
        if (ret < 0) {
            LOG(LOG_ERROR, "Error: failed to send event (errno: %s)", strerror(ret));
        }
        ret = rt_event_signal(&wheel_manager_event, EVENT_ACQUIRE_SENSOR);
        if (ret < 0) {
            LOG(LOG_ERROR, "Error: failed to send event (errno: %s)", strerror(ret));
        }

    }
    LOG(LOG_INFO, "end of period_manager_task_main");
}

//--------------------------------------------
// wheel_manager_task task functions           
//--------------------------------------------

void wheel_manager_task_main()
{
    LOG(LOG_INFO, "start of wheel_manager_task_main");
    ulong mask_value = 0;

    SampleType sample;
    while (true) {
        int ret = rt_event_wait(&wheel_manager_event, EVENT_UPDATE_WHEEL_CONTROL | EVENT_ACQUIRE_SENSOR | EVENT_EXPERIENCE_FINISHED | EVENT_INIT_LAW, &mask_value, EV_ANY, TM_INFINITE);
        if (ret < 0) {
            LOG(LOG_ERROR, "Error: Failed to wait on event (ernno: %s)", strerror(ret));
        }
        // Process events
        if ((mask_value & EVENT_INIT_LAW) == EVENT_INIT_LAW) {
            LOG(LOG_DEBUG, "Processing EVENT_INIT_LAW");
            SampleAcquisition(&sample);
            InitializeExperiment(sample);

            rt_event_clear(&wheel_manager_event, EVENT_INIT_LAW, NULL);
        }
        if ((mask_value & EVENT_EXPERIENCE_FINISHED) == EVENT_EXPERIENCE_FINISHED) {
            LOG(LOG_DEBUG, "Processing EVENT_EXPERIENCE_FINISHED");
            finished_flag = true;
            brake_and_reset_motor();

            rt_event_clear(&wheel_manager_event, EVENT_EXPERIENCE_FINISHED, NULL);
        }
        if ((mask_value & EVENT_UPDATE_WHEEL_CONTROL) == EVENT_UPDATE_WHEEL_CONTROL) {
            LOG(LOG_DEBUG, "Processing EVENT_UPDATE_WHEEL_CONTROL");
            SampleAcquisition(&sample);
            float current_command = ComputeLaw(sample);
            ApplySetpointCurrent(current_command);

            rt_event_clear(&wheel_manager_event, EVENT_UPDATE_WHEEL_CONTROL, NULL);
        }
        if ((mask_value & EVENT_ACQUIRE_SENSOR) == EVENT_ACQUIRE_SENSOR) {
            LOG(LOG_DEBUG, "Processing EVENT_ACQUIRE_SENSOR");
            SampleAcquisition(&sample);

            rt_queue_write(&sample_queue, &sample, sizeof (sample), Q_NORMAL);
            rt_event_clear(&wheel_manager_event, EVENT_ACQUIRE_SENSOR, NULL);
        }

        mask_value = 0;
    }

    LOG(LOG_INFO, "end of period_manager_task_main");
}
//--------------------------------------------------------------------

/* Linux Signals Handler */
void stopNow(int sig)
{
    rt_sem_v(&ExitApplication_Semaphore); // give stop semaphore to main task 
}

//--------------------------------------------------------------------
// 
//  main()
// 
//  Description: create and kill all the application's components
// 
//--------------------------------------------------------------------

int main(int argc, char* argv[])
{
    LOG_LEVEL = LOG_INFO;

    // Kernel and peripherals initialization
    HardwareInitialize();
    LOG(LOG_INFO, "---- Starting Realtime Application ----");

    // Signal manager to stop the application 
    signal(SIGINT, stopNow); // Interrupt from keyboard 
    signal(SIGTERM, stopNow); // Termination signal 

    // Global variables initialization       
    // -------------------------------
    // *** This space must be completed  if needed   *****


    // ------------------------------------- 
    // Events creation               
    rt_event_create(&wheel_manager_event, "wheel_manager_event", 0, EV_PRIO);

    // Message queues creation               
    // -------------------------------------
    rt_queue_create(&sample_queue, "sample_queue", sizeof (SampleType) * QUEUE_MAX_SIZE, QUEUE_MAX_SIZE, Q_FIFO);

    // Semaphores creation                 
    // ------------------------------------
    rt_sem_create(&StartExperiment_Semaphore, "Start", 0, S_FIFO);
    rt_sem_create(&ExitApplication_Semaphore, "Exit", 0, S_FIFO);


    // Mutual exclusion semaphore creation                     
    //---------------------------------------------------------
    // rt_mutex_create(&myMutex, "mx" );                       
    //    **** This space must be completed  if needed   ***** 

    // Tasks creation                                          
    //---------------------------------------------------------
    rt_task_create(&request_manager_task, "request_manager", DEFAULTSTACKSIZE, REQUEST_MANAGER_INTERFACE_PRIORITY, 0);
    rt_task_create(&period_manager_task, "period_manager", DEFAULTSTACKSIZE, PERIOD_MANAGER_INTERFACE_PRIORITY, 0);
    rt_task_create(&wheel_manager_task, "wheel_manager_task", DEFAULTSTACKSIZE, WHEEL_MANAGER_INTERFACE_PRIORITY, 0);

    // Tasks starting
    rt_task_start(&request_manager_task, &request_manager_task_main, NULL);
    rt_task_start(&period_manager_task, &period_manager_task_main, NULL);
    rt_task_start(&wheel_manager_task, &wheel_manager_task_main, NULL);

    //-----------------------------------------------------------
    // Main Task waits on exit semaphore                          
    //-----------------------------------------------------------
    rt_sem_p(&ExitApplication_Semaphore, TM_INFINITE);

    //-----------------------------------------------------------
    // Tasks destruction                                         
    //-----------------------------------------------------------
    // rt_task_ delete(&myTask);                                 
    LOG(LOG_INFO, "  Destruction of   - Tasks  - Queues  - Semaphores  - Events  - Mutexes");
    // **** This space must be completed *****  
    rt_task_delete(&request_manager_task);
    rt_task_delete(&period_manager_task);
    rt_task_delete(&wheel_manager_task);

    //------------------------------------------------------------
    // Semaphores destruction                                     
    //-------------------------------------------------------------
    rt_sem_delete(&StartExperiment_Semaphore);
    rt_sem_delete(&ExitApplication_Semaphore);

    //------------------------------------------------------------
    // Events destruction                                 
    //------------------------------------------------------------
    rt_event_delete(&wheel_manager_event);

    //------------------------------------------------------------
    // Message queues destruction                                 
    //------------------------------------------------------------
    rt_queue_delete(&sample_queue);


    LOG(LOG_INFO, " Application ..... finished--> exit");
    // Peripherals uninitialization 
    HardwareTerminate();
    return (0);
}
