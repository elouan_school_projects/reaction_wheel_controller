// ***********************************************************
// *                I S A E - S U P A E R O                  *
// *                                                         *
// *               Reaction Wheel Application                *
// *                      Version 2021                       *
// *                                                         *
// * Student version                                         *
// *******

#include <sys/time.h>
#include <stdio.h> 


char* get_current_time(void)
{
    static char formatted_time[100];
    struct timeval time;

    gettimeofday(&time, NULL);
    snprintf(formatted_time, 100, "%lu::%lu", time.tv_sec, time.tv_usec/1000);

    return formatted_time;
}
