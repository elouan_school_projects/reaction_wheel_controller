// ***********************************************************
// *                I S A E - S U P A E R O                  *
// *                                                         *
// *               Reaction Wheel Application                *
// *                      Version 2021                       *
// *                                                         *
// * Student version                                         *
// ***********************************************************

#ifndef LOG_H
#define LOG_H

#include <stdio.h>

#define LOG_DEBUG 1   /* Use for debuging messages */
#define LOG_INFO 2    /* Use for basic execution message */
#define LOG_WARNING 3 /* Use for non blocking errors */
#define LOG_ERROR 4   /* Use for critical errors */
#ifndef DEFAULT_LOG_LEVEL
#define DEFAULT_LOG_LEVEL LOG_INFO
#endif

#define COLOR_NRM "\x1B[0m"  /* Reset style */
#define COLOR_WHT "\x1B[37m" /* Color white */
#define COLOR_RED "\x1B[31m" /* Color red */
#define COLOR_GRN "\x1B[32m" /* Color green */
#define COLOR_BLU "\x1B[34m" /* Color blue */
#define COLOR_YEL "\x1B[33m" /* Color yellow */
#define COLOR_MAG "\x1B[35m" /* Color magenta */
#define COLOR_CYN "\x1B[36m" /* Color cyan */

static int LOG_LEVEL = DEFAULT_LOG_LEVEL;


// Returns the local date/time formatted as H:M:S::MS
char* get_current_time(void);

#ifdef NO_LOG
#define LOG(level, ...) \
    do {                \
    } while (0)
#else
#define LOG(level, ...)                                                                             \
    do {                                                                                            \
        if (level >= LOG_LEVEL) {                                                                   \
            switch (level) {                                                                        \
                case LOG_DEBUG:                                                                     \
                    rt_printf("%s[DEBUG] %s-%s:%d:%s()\t", COLOR_NRM, get_current_time(), __FILE__, __LINE__, __func__);   \
                    rt_printf(__VA_ARGS__);                                                         \
                    rt_printf("%s\r\n", COLOR_NRM);                                                   \
                    break;                                                                          \
                case LOG_INFO:                                                                      \
                    rt_printf("%s[INFO] %s-%s:%d:%s()\t", COLOR_BLU, get_current_time(), __FILE__, __LINE__, __func__);    \
                    rt_printf(__VA_ARGS__);                                                         \
                    rt_printf("%s\r\n", COLOR_NRM);                                                   \
                    break;                                                                          \
                case LOG_WARNING:                                                                   \
                    rt_printf("%s[WARNING] %s-%s:%d:%s()\t", COLOR_MAG, get_current_time(), __FILE__, __LINE__, __func__); \
                    rt_printf(__VA_ARGS__);                                                         \
                    rt_printf("%s\r\n", COLOR_NRM);                                                   \
                    break;                                                                          \
                case LOG_ERROR:                                                                     \
                    rt_printf("%s[ERROR] %s-%s:%d:%s()\t", COLOR_RED, get_current_time(), __FILE__, __LINE__, __func__);   \
                    rt_printf(__VA_ARGS__);                                                         \
                    rt_printf("%s\r\n", COLOR_NRM);                                                   \
                    break;                                                                          \
                default:                                                                            \
                    break;                                                                          \
            }                                                                                       \
        }                                                                                           \
    } while (0)
#endif

#endif /* LOG_H */